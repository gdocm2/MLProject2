B
    ���\�  �               @   s�   d Z ddlZddlZddlmZ dgZG dd� de�Zdd� Z	dd	� Z
d
d� Zdd� Zdd� Zdd� Zdd� Ze	e
dd�Ze	edd�Ze	edd�Ze	edd�Ze	edd�Ze	edd�Zeeeeeed�ZdS )z�Metrics to evaluate the fitness of a program.

The :mod:`gplearn_MLAA.fitness` module contains some metric with which to evaluate
the computer programs created by the :mod:`gplearn_MLAA.genetic` module.
�    N)�rankdata�make_fitnessc               @   s    e Zd ZdZdd� Zdd� ZdS )�_FitnessaF  A metric to measure the fitness of a program.

    This object is able to be called with NumPy vectorized arguments and return
    a resulting floating point score quantifying the quality of the program's
    representation of the true relationship.

    Parameters
    ----------
    function : callable
        A function with signature function(y, y_pred, sample_weight) that
        returns a floating point number. Where `y` is the input target y
        vector, `y_pred` is the predicted values from the genetic program, and
        sample_weight is the sample_weight vector.

    greater_is_better : bool
        Whether a higher value from `function` indicates a better fit. In
        general this would be False for metrics indicating the magnitude of
        the error, and True for metrics indicating the quality of fit.

    c             C   s   || _ || _|rdnd| _d S )N�   �����)�function�greater_is_better�sign)�selfr   r   � r   �:/Users/rizzoli/PycharmProjects/ml2/gplearn_MLAA/fitness.py�__init__*   s    z_Fitness.__init__c             G   s
   | j |� S )N)r   )r
   �argsr   r   r   �__call__/   s    z_Fitness.__call__N)�__name__�
__module__�__qualname__�__doc__r   r   r   r   r   r   r      s   r   c             C   sz   t |t�stdt|� ��| jjdkr6td| jj ��t | t�ddg�t�ddg�t�ddg��tj	�sptd��t
| |�S )a+  Make a fitness measure, a metric scoring the quality of a program's fit.

    This factory function creates a fitness measure object which measures the
    quality of a program's fit and thus its likelihood to undergo genetic
    operations into the next generation. The resulting object is able to be
    called with NumPy vectorized arguments and return a resulting floating
    point score quantifying the quality of the program's representation of the
    true relationship.

    Parameters
    ----------
    function : callable
        A function with signature function(y, y_pred, sample_weight) that
        returns a floating point number. Where `y` is the input target y
        vector, `y_pred` is the predicted values from the genetic program, and
        sample_weight is the sample_weight vector.

    greater_is_better : bool
        Whether a higher value from `function` indicates a better fit. In
        general this would be False for metrics indicating the magnitude of
        the error, and True for metrics indicating the quality of fit.

    z&greater_is_better must be bool, got %s�   z5function requires 3 arguments (y, y_pred, w), got %d.r   �   zfunction must return a numeric.)�
isinstance�bool�
ValueError�type�__code__�co_argcount�np�array�numbers�Numberr   )r   r   r   r   r   r   3   s    
c          
   C   s�   t jddd��~ |t j||d� }| t j| |d� }t �|| | �t �|� t �t �||d  �t �||d  � t �|�d  � }W dQ R X t �|�r�t �|�S dS )z7Calculate the weighted Pearson correlation coefficient.�ignore)�divide�invalid)�weightsr   Ng        )r   �errstate�average�sum�sqrt�isfinite�abs)�y�y_pred�wZy_pred_demeanZy_demean�corrr   r   r   �_weighted_pearsonY   s    

r.   c             C   s(   t �td|�}t �td| �}t|||�S )z8Calculate the weighted Spearman correlation coefficient.r   )r   �apply_along_axisr   r.   )r*   r+   r,   Zy_pred_rankedZy_rankedr   r   r   �_weighted_spearmang   s    r0   c             C   s   t jt �||  �|d�S )z"Calculate the mean absolute error.)r#   )r   r%   r)   )r*   r+   r,   r   r   r   �_mean_absolute_errorn   s    r1   c             C   s   t j||  d |d�S )z Calculate the mean square error.r   )r#   )r   r%   )r*   r+   r,   r   r   r   �_mean_square_errors   s    r2   c             C   s   t �t j||  d |d��S )z%Calculate the root mean square error.r   )r#   )r   r'   r%   )r*   r+   r,   r   r   r   �_root_mean_square_errorx   s    r3   c             C   s\   d}t �d| |d| �}t �||d| �}| t �|� d|  t �|�  }t j| |d�S )zCalculate the log loss.gV瞯�<r   )r#   )r   �clip�logr%   )r*   r+   r,   �epsZ
inv_y_pred�scorer   r   r   �	_log_loss}   s
     r8   T)r   r   F)�pearson�spearmanzmean absolute error�mse�rmsezlog loss)r   r   �numpyr   �scipy.statsr   �__all__�objectr   r   r.   r0   r1   r2   r3   r8   Zweighted_pearsonZweighted_spearman�mean_absolute_errorZmean_square_errorZroot_mean_square_error�log_loss�_fitness_mapr   r   r   r   �<module>   s:    &	